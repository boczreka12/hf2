import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom'
import './App.css';
import FirstPage from './components/FirstPage'
import LearnMore from './components/LearnMore'


class App extends Component {
  render() {
    return (
      <div className="App">
       <BrowserRouter>
          <Route path='/' exact component={FirstPage}></Route>   
          <Route path='/learnmore' exact component={LearnMore}></Route>
       </BrowserRouter>
      </div>
    );
  }
}

export default App;
