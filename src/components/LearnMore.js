/* 
  Ez, a részletes leírást tartalmazó oldal jelenítsen meg nagyobb méretben egy avatárképet 
  és egy felhasználó keresztnevet, illetve tartalmazzon egy input mezőt és egy gombot

 */
import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import Card from '@material-ui/core/Card';
import { CardImg, CardTitle} from 'reactstrap';
import Box from '@material-ui/core/Box';
import Faker from 'faker';
import Button from  '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

class LearnMore extends Component{
  constructor (props){
    super();
    this.inputText = ''
    this.state ={
        enteredText : ''
    }
  }  
    render(){
        return(
            <div>
                <Card >
                     <Box>
                            <CardImg src={Faker.image.avatar()} alt="picture" style={{ width: 500 }} />
                     </Box>
                  
                   <Box style={{ padding: "15px" }}> 
                            <CardTitle className="CardTitle">{Faker.name.firstName()}</CardTitle>
                    </Box>
                    <Box>
                            <Button onClick={() => this.writeInput()} >
                                   gomb
                            </Button>
                            <Input onChange={ (event) => this.setText(event.target.value)}> 
                                   {this.inputText}
                            </Input>
                    </Box>

                </Card>
                <p>
                    {this.state.enteredText}
                </p>
                <Link to='/'>
                    Previous Page
                </Link>
            </div>
        )
    }
    writeInput(){
        this.setState( {enteredText : this.inputText}) ;
        console.log(this.state.enteredText); 
        //this.setText('a');
        //console.log(this.inputText);
    }

    setText(szoveg){
        this.inputText = szoveg

    }
}
export default LearnMore; 