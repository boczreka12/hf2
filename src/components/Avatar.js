import React, {Component} from 'react'
import Card from '@material-ui/core/Card';
import { CardImg, CardTitle} from 'reactstrap';
import Box from '@material-ui/core/Box';
import {Link} from 'react-router-dom';

class Avatar extends Component {
    constructor(props){
        super();
    }
    render(){
        return(
            <div>
                <Card className="Card">
                     <Box>
                            <CardImg src={this.props.picture} alt="picture" style={{ width: 150 }} />
                     </Box>
                  
                   <Box style={{ padding: "15px" }}>
                       <Link to='/learnmore'>
                                 <CardTitle className="CardTitle">{this.props.name}</CardTitle>
                        </Link>                          
                    </Box>
                </Card>
            </div>
        )
    }
}
export default Avatar;